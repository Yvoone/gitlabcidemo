#!/usr/bin/env bash

mkdir target/dependency
(cd target/dependency; jar -xf ../*.jar)
docker build -t com.bikebook/temperatureservice .