package com.mangroo.temperature.integration;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class GetHealthSteps {
    private ResponseEntity<String> response; // output

    @LocalServerPort
    int randomServerPort;

    private final TestRestTemplate template = new TestRestTemplate();

    @When("the client checks the health of the application")
    public void the_client_issues_GET_health() {
        log.info("randomServerPort {}", randomServerPort);
        final String baseUrl = "http://localhost:" + randomServerPort;
        response = template.getForEntity(baseUrl + "/actuator/health", String.class);
    }

    @Then("^the application is up$")
    public void the_client_receives_status_code_of() throws Throwable {
        HttpStatus currentStatusCode = response.getStatusCode();
        assertThat(currentStatusCode).isEqualTo(HttpStatus.OK);
    }

}